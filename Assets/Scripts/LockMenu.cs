﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockMenu : MonoBehaviour
{
        StickyUIMenu StickyUIMenuInstance;
        public GameObject mimicPlayerFaceCube, UIMenu, LookAtPlayerFace;
        public Quaternion lookAtRotation;
        // public Transform LookAtPlayerFace;
        public float followX, followY, followZ;
        // public bool locked = false;
        // public bool locked = false;
    // Start is called before the first frame update
    void Start()
    {
        StickyUIMenuInstance = FindObjectOfType <StickyUIMenu>();
    }

    // Update is called once per frame
    void Update()
    {
        // //basically works, strange swivel when player moves after pause, rotation not locked
        // if(StickyUIMenuInstance.inMenu)
        // {
        //     UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
        //     // UIMenu.transform.LookAt(LookAtPlayerFace, transform.up);
        //     Vector3 relativePos = LookAtPlayerFace.transform.position;
        //     // the second argument, upwards, defaults to Vector3.up
        //     Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        //     UIMenu.transform.rotation = rotation;
        // }
        // else
        // {
        //     locked = false;
        //     followX = mimicPlayerFaceCube.transform.position.x;
        //     followY = mimicPlayerFaceCube.transform.position.y ;
        //     followZ = mimicPlayerFaceCube.transform.position.z ;
        // }

        //working better but moving heights for player messes up menu plane
        if(StickyUIMenuInstance.inMenu)
        {
            UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
            // UIMenu.transform.TransformPoint(followX,followY,followZ);
            // UIMenu.transform.rotation = lookAtRotation;
            UIMenu.transform.rotation = Quaternion.Euler(lookAtRotation.x, lookAtRotation.y, 0.0f);
        }
        else
        {
            followX = mimicPlayerFaceCube.transform.position.x;
            followY = mimicPlayerFaceCube.transform.position.y ;
            followZ = mimicPlayerFaceCube.transform.position.z ;
            
            lookAtRotation = mimicPlayerFaceCube.transform.rotation;
            // lookAtRotation = lookAtRotation - 
        }


        // if(StickyUIMenuInstance.inMenu)
        // {
        //     UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
        //     UIMenu.transform.TransformPoint(followX,followY,followZ);
        //     UIMenu.transform.rotation = lookAtRotation;
        // }
        // else
        // {
        //     followX = mimicPlayerFaceCube.transform.position.x;
        //     followY = mimicPlayerFaceCube.transform.position.y ;
        //     followZ = mimicPlayerFaceCube.transform.position.z ;

        //     lookAtRotation = mimicPlayerFaceCube.GetComponent<Rigidbody>().rotation;
            
        //     Vector3 relativePos.transform.TransformPoint(LookAtPlayerFace.transform.position);
        //     lookAtRotation = Quaternion.LookRotation(relativePos, Vector3.up);
        // }
    }
}
