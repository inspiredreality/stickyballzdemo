﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnDestroyerR : MonoBehaviour
{
    // public float respawnDelay;
    public float Timer;

    void Update()
    {
        Timer += Time.deltaTime;

        if (Timer > GenerateObjects.instance.respawnDelayR){
            // send vacant message to GenerateObjectController
            GenerateObjects.instance.spawnDestroyerR = true;
            Timer = 0;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.name.Contains("Destroyer")){
            Timer = 0;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.name.Contains("Destroyer")){
            Timer = 0;
        }
    }
}
