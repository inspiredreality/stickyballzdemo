﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBallForce : MonoBehaviour
{
    public float push = .5f;
    public Vector3 velocityReading;
    public string directionHeaded;
    public GameObject SpawnBall;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // directionHeaded = "Zpos";
    }

    void FixedUpdate()
    {
        //push in correct direction
        switch (directionHeaded)
        {
            case "Zpos":
                rb.AddForce(0,0,push);
                break;
            case "Xneg":
                rb.AddForce(-push,0,0);
                break;
            case "Zneg":
                rb.AddForce(0,0,-push);
                break;                            
            case "Xpos":
                rb.AddForce(push,0,0);;
                break;       
            default:
                break;                      
        }

        // if(directionHeaded == "Zpos")
        // {
        //     rb.AddForce(0,0,push);
        // }

        //set top spee/velocity for all directions
        if(rb.velocity.x >= push)
        {
            rb.velocity = new Vector3(push, rb.velocity.y, rb.velocity.z);
        }
        if(rb.velocity.y >= push)
        {
            rb.velocity = new Vector3(rb.velocity.x, push, rb.velocity.z);
        }
        if(rb.velocity.z >= push)
        {
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, push);
        }
        if(rb.velocity.x <= -push)
        {
            rb.velocity = new Vector3(-push, rb.velocity.y, rb.velocity.z);
        }
        if(rb.velocity.y <= -push)
        {
            rb.velocity = new Vector3(rb.velocity.x, -push, rb.velocity.z);
        }
        if(rb.velocity.z <= -push)
        {
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, -push);
        }

        velocityReading = rb.velocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if(collision.collider.name == "TurnXneg" )
        // {
            switch (collision.collider.name)
            {
                case "TurnXneg":
                    directionHeaded = "Xneg";
                    rb.velocity.Set(0,0,0);
                    break;
                case "TurnZneg":
                    directionHeaded = "Zneg";
                    rb.velocity.Set(0,0,0);
                    break; 
                case "TurnXpos":
                    directionHeaded = "Xpos";
                    rb.velocity.Set(0,0,0);
                    // if(gameobject.name != "StarterBall")
                    // {
                        Instantiate(SpawnBall, new Vector3(1.996f, 1.872f, -2.936f), new Quaternion(0,0,0,0));
                    // }
                    break;
                case "TurnZpos":
                    directionHeaded = "Zpos";
                    rb.velocity.Set(0,0,0);
                    break;
                case "Destroy":
                    Destroy(gameObject);
                    break;
                default:
                    break;                      
            }

            // directionHeaded = "Xneg";
            // GetComponent<Rigidbody>().isKinematic = true;
            // StartCoroutine(StickyTimerStarted());
        // }

    }


}
