﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateMarker : MonoBehaviour
{
    public GameObject smallBallMarker, bigBallMarker, smallCubeMarker, bigCubeMarker, 
    smallCylinderMarker, bigCylinderMarker, smallPyramidMarker, bigPyramidMarker;
    public bool canInstantiate = true;
    //wont work for PlayerEnvironmentController bc we have multiple PlayerEnvironmentControllers, 1 on each ball
    // ARFGameController m_ARFGameController;

    void Awake()
    {
        // m_ARFGameController = FindObjectOfType <ARFGameController>();
        
    }

    private void OnCollisionExit(Collision collision)
    {        
        // if(collision.PlayerEnvironmentController.GetBallStatus() == "Attract")
        if(collision.collider.tag == "Attract" && canInstantiate)
        {
            canInstantiate = false;

            string thrownObjectName = collision.collider.name;

            if (thrownObjectName.Contains("big"))
            {
                if (thrownObjectName.Contains("Ball"))
                {
                    var newMarker = Instantiate(bigBallMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cube"))
                {
                    var newMarker = Instantiate(bigCubeMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cylinder"))
                {
                    var newMarker = Instantiate(bigCylinderMarker, collision.transform.position, collision.transform.rotation);
                }            
                else if(thrownObjectName.Contains("Pyramid"))
                {
                    var newMarker = Instantiate(bigPyramidMarker, collision.transform.position, collision.transform.rotation);
                }            
            }
            else if(thrownObjectName.Contains("small"))
            {
                if (thrownObjectName.Contains("Ball"))
                {
                    var newMarker = Instantiate(smallBallMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cube"))
                {
                    var newMarker = Instantiate(smallCubeMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cylinder"))
                {
                    var newMarker = Instantiate(smallCylinderMarker, collision.transform.position, collision.transform.rotation);
                }            
                else if(thrownObjectName.Contains("Pyramid"))
                {
                    var newMarker = Instantiate(smallPyramidMarker, collision.transform.position, collision.transform.rotation);
                }
            }

            // Debug.Log("OnCollisionExit Sticky  "  + collision.transform.tag);
            // Debug.Log("OnCollisionExit Sticky  " + collision.transform.name);
        
            
            // if(collision.collider.name == "bigBall1" || collision.collider.name == "bigBall2")
            // {
            //     var newMarker = Instantiate(bigMarker, collision.transform.position, collision.transform.rotation);
            //     // newMarker.gameObject.tag = "LoseCondition";
            //     Debug.Log("Big");
            // }
            // else
            // {
            //     var newMarker = Instantiate(smallMarker, collision.transform.position, collision.transform.rotation);
            //     // newMarker.gameObject.tag = "LoseCondition";
            //     Debug.Log("Small");
            // }

            // Debug.Log("StartPaused");

            StartCoroutine(PauseMarkerInstantiation());
            // Debug.Log("EndPaused");

            // canInstantiate = true;
        }
    }

    IEnumerator PauseMarkerInstantiation()
    {
        yield return new WaitForSeconds(2);
            Debug.Log("EndPausedinRoutine");
        canInstantiate = true;
    } 
}
