﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectTimer : MonoBehaviour
{
    public int delayDestroySecs = 3;
    void Update()
    {
        StartCoroutine(SpawnDestroyerTimer());
    }
    IEnumerator SpawnDestroyerTimer()
    {
        // audioData.Play();
        yield return new WaitForSeconds(delayDestroySecs);
        Destroy(gameObject);
    } 

}
