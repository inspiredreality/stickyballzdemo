﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnDestroyerL : MonoBehaviour
{
    // public float respawnDelay;
    public float Timer;

    // Update is called once per frame
    void Update()
    {

    Timer += Time.deltaTime;

        if (Timer > GenerateObjects.instance.respawnDelayL){
            // send vacant message to GenerateObjectController
            GenerateObjects.instance.spawnDestroyerL = true;
            Timer = 0;
            // StartCoroutine(SpawnDestroyerDelay(1));
        }
        
    }

    // private void OnTriggerExit(Collider other)
    // {
    //     timer += 3;

    //     if(gameobeject.name.Contains("Left"))
    //     {   
            
    //     }
    //     else if(gameobeject.name.Contains("Right"))
    //     {   

    //     }
    // }

    //     IEnumerator SpawnDestroyerDelay(int waitFor)
    // {
    //     yield return new WaitForSeconds(waitFor);
    // } 

    void OnTriggerStay(Collider other)
    {
        if(other.name.Contains("Destroyer")){
            Timer = 0;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.name.Contains("Destroyer")){
            Timer = 0;
        }
    }

}
