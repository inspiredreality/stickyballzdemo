﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockAxis : MonoBehaviour
{
        public GameObject parentPlayer;
        public Quaternion parentRotation;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        parentRotation = parentPlayer.transform.rotation;
        // transform.rotation = -parentRotation.z;

        transform.rotation = Quaternion.Euler(parentRotation.x, parentRotation.y, 0.0f);
    }
}
