﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingSpaceObjectController : MonoBehaviour
{
    Rigidbody m_Rigidbody;
    public float m_Speed = 1f;

    GameObject AttractionPoint;
    // public AudioSource attractingSound;
    public GameObject explodingObject;

    public Vector3 velocityReading;

    public float gravitationalAcceleration, smallThrowableAccel = 11, bigThrowableAccel = 25;
    private float throwableAccel;


    // Start is called before the first frame update
    void Start()
    {
        AttractionPoint = GameObject.FindGameObjectWithTag("AttractionPoint");

        if(transform.name.Contains("small"))
        {
            throwableAccel = smallThrowableAccel;
        }
        else if (transform.name.Contains("big")) 
        {
            throwableAccel = bigThrowableAccel;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // GetComponent<Rigidbody>().velocity = -transform.forward * m_Speed;
        transform.position += -Vector3.forward * Time.deltaTime * throwableAccel;

        // GetComponent<Rigidbody>().velocity += gravitationalAcceleration * throwableAccel * (AttractionPoint.transform.position - transform.position);
        // GetComponent<Rigidbody>().velocity = throwableAccel * (AttractionPoint.transform.position - transform.position);

        // gameObject.transform.position = Vector3.Slerp(gameObject.transform.position, AttractionPoint.transform.position, Time.deltaTime * gravitationalAcceleration);
        velocityReading = GetComponent<Rigidbody>().velocity;
        //use mathf.Abs to set -5/5 max

        // if(!attractingSound.isPlaying){
        //     attractingSound.Play();
        // }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "DestroyObject" )
        {   
            //explode ball
            // rend.material.color = Color.HSVToRGB(9, 77, 77);
            // attractingSound.Stop();
            Instantiate(explodingObject, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        else if(other.gameObject.tag == "DisappearObject" )
        {   
            //make ball disappear
            Destroy(gameObject);
        }

    }


}
