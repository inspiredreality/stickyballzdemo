﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public static GameManager GMinstance;
    public GameObject ThrowablesHierarchy;
    public string totalScore, ballsLost, realTime, thisLevelName, nextLevel;
    //constant or calculated at start of level
    public float stickySecStartSmall = 3, stickySecStartBig = 5, winningScore;
    public int lostBallPenalty, nextLevelIndex, startingThrowables;
    //reset between levels
    public float totalScoreRaw, bigBallScore, bigCubeScore, bigCylinderScore, bigPyramidScore, 
        smallBallScore, smallCubeScore, smallCylinderScore, smallPyramidScore, realTimeRaw;

    public int ballsLostRaw;
    public bool paused = false, WinCondition = false, LoseCondition = false;
    // public bool playing = true, paused = false, WinCondition = false, LoseCondition = false;
    //playing=false is level won, or level lost.

    void Awake()
    {
        if (GMinstance == null)
        {
            //assign it to current object
            GMinstance = this;
        }
        //make sure it is the current object
        else if (GMinstance != this)
        {
            //destroy game object, we only need 1 and it already exists
            Destroy(gameObject);
        }
        //keep alive, dont destroy when scene changes
        DontDestroyOnLoad(gameObject);

        //Create array of floats for scores out of all objects tagged "Ball" instead of delaring as public floats 
        ThrowablesHierarchy = GameObject.Find("Throwables");
        startingThrowables = ThrowablesHierarchy.transform.childCount;
    }

    void Update()
    {
        convertRawScores();

        if(totalScoreRaw >= winningScore)
        {
            WinCondition = true;
            // pauseGame();
            //win game confetti 
        }
        if(ballsLostRaw == startingThrowables)
        {
            // LoseCondition = true;
            StartCoroutine(LoseTimerStarted());
            // pauseGame();
            //LOSER 
        }
        

        // if(!playing)
        // {
        //     pauseGame();

        //     //check win or loss
        // }

        // //set by player controller 
        // if(LoseCondition)
        // {
        //     //you lose
        //     pauseGame();
        // }

        if(WinCondition || LoseCondition)
        {
            pauseGame();
        }
    }

    IEnumerator LoseTimerStarted()
    {
        // audioData.Play();
        yield return new WaitForSeconds(2);
        LoseCondition = true;
    } 

    private void convertRawScores(){
        totalScoreRaw = (bigBallScore + bigCubeScore + bigCylinderScore + bigPyramidScore + smallBallScore + 
        smallCubeScore + smallCylinderScore + smallPyramidScore) - ballsLostRaw*lostBallPenalty;
        realTimeRaw += Time.deltaTime;
        // totalScore = Mathf.FloorToInt((totalScoreRaw * 100f) % 100f);

        //send numbers as string to UI
        int ScoreSeconds = Mathf.FloorToInt(totalScoreRaw % 60f);
        int ScoreMilliseconds = Mathf.Abs(Mathf.FloorToInt((totalScoreRaw * 100f) % 100f));
        // totalScore = ScoreSeconds.ToString ("00") + "." + ScoreMilliseconds.ToString("00") + " / " + winningScore.ToString();
        totalScore = Mathf.FloorToInt(totalScoreRaw).ToString() + " / " + winningScore.ToString();
        ballsLost = ballsLostRaw.ToString(); 
        int realMinutes = Mathf.FloorToInt(realTimeRaw / 60f);
        int realSeconds = Mathf.FloorToInt(realTimeRaw % 60f);
        realTime = realMinutes.ToString ("00") + ":" + realSeconds.ToString ("00");
    }
    
    public void pauseGame()
    {
        if(!WinCondition && !LoseCondition)
        {
            if(paused)
            {
                Time.timeScale = 1;
                paused = false;
            }
            else
            {
                Time.timeScale = 0;
                paused = true;            
            }
        }

        if(WinCondition || LoseCondition)
        {
            //use Chronos to only pause balls and not confetti 
            Time.timeScale = 0;
            paused = true;    

            // Scene scene = SceneManager.GetActiveScene();
            // thisLevelName = scene.name;
            // var nextLevelIndex = scene.buildIndex + 1;
        }

    }

    public void clearScores()
    {
        totalScoreRaw = bigBallScore = bigCubeScore = bigCylinderScore = bigPyramidScore = smallBallScore = 
        smallCubeScore = smallCylinderScore = smallPyramidScore = winningScore = realTimeRaw = ballsLostRaw = 0;
        paused = WinCondition = LoseCondition = false;
        convertRawScores();
    }
}
